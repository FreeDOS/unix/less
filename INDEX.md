# less

Less is a program similar to more, but which allows backward movement in the file as well as forward movement. Also, less does not have to read the entire input file before starting, so with large input files it starts up faster than text editors like vi.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LESS.LSM

<table>
<tr><td>title</td><td>less</td></tr>
<tr><td>version</td><td>291a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-02-01</td></tr>
<tr><td>description</td><td>Page viewer with backward movement</td></tr>
<tr><td>keywords</td><td>less,more,pager</td></tr>
<tr><td>author</td><td>Mark Nudelmam</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Simplified (2-Clause) BSD License</td></tr>
<tr><td>summary</td><td>Less is a program similar to more, but which allows backward movement in the file as well as forward movement. Also, less does not have to read the entire input file before starting, so with large input files it starts up faster than text editors like vi.</td></tr>
</table>
